package main

import (
	"fmt"
	"gitlab.com/Ma_124/word-encoding"
	"io/ioutil"
	"os"
)

func main() {
	var word string
	var inp []byte
	var err error
	switch len(os.Args) {
	case 1:
		word = "computer"
		inp, err = ioutil.ReadAll(os.Stdin)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	case 2:
		word = "computer"
		inp = []byte(os.Args[1])
	case 3:
		word = os.Args[2]
		inp = []byte(os.Args[1])
	default:
		fmt.Println("word-encoder [inp] [word]")
		os.Exit(1)
	}
	e := &word_encoding.Enc{Word: word}
	fmt.Printf("\n\n%s\n", e.EncodeString(inp))
}
