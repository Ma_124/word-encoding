package word_encoding

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var e = &Enc{
	Word: "computer",
}

func TestEnc_EncodeChar(t *testing.T) {
	tests := []struct {
		name string
		b    byte
		want string
	}{
		{
			"0x00",
			0x00,
			"computer",
		},
		{
			"0x55",
			0x55,
			"cOmPuTeR",
		},
		{
			"0xFF",
			0xFF,
			"COMPUTER",
		},
		{
			"A",
			'A',
			"cOmputeR",
		},
	}
	e := &Enc{
		Word: "computer",
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, string(e.EncodeChar(tt.b)))
		})
	}
}

func TestEnc_DecodeChar(t *testing.T) {
	tests := []struct {
		name string
		want byte
		str  string
	}{
		{
			"0x00",
			0x00,
			"computer",
		},
		{
			"0x55",
			0x55,
			"cOmPuTeR",
		},
		{
			"0xFF",
			0xFF,
			"COMPUTER",
		},
		{
			"A",
			'A',
			"cOmputeR",
		},
	}
	e := &Enc{
		Word: "computer",
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, e.DecodeChar([]byte(tt.str)))
		})
	}
}

func TestEnc_EncodeString(t *testing.T) {
	tests := []struct {
		name string
		str  string
		want string
	}{
		{
			"0x00",
			string([]byte{0x00, 0x55, 0xFF}),
			"computercOmPuTeRCOMPUTER",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, string(e.EncodeString([]byte(tt.str))))
		})
	}
}

func TestEnc_DecodeChar_invalidLen(t *testing.T) {
	defer func() {
		assert.Equal(t, "invalid length", recover())
	}()
	e.DecodeChar([]byte{0xFF})
}

func TestEnc_DecodeString_invalidLen(t *testing.T) {
	defer func() {
		assert.Equal(t, "invalid length", recover())
	}()
	e.DecodeString([]byte("abcdefghi"))
}

func TestEnc_DecodeString(t *testing.T) {
	tests := []struct {
		name string
		want string
		str  string
	}{
		{
			"0x00",
			string([]byte{0x00, 0x55, 0xFF}),
			"computercOmPuTeRCOMPUTER",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, string(e.DecodeString([]byte(tt.str))))
		})
	}
}
