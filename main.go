package word_encoding

type Enc struct {
	Word string
}

func caseByBool(r byte, bit bool) byte {
	if bit {
		return r - ' '
	}
	return r
}

func maskByI(i uint) byte {
	return byte(1 << (7 - i))
}

func (e *Enc) EncodeChar(b byte) []byte {
	ret := make([]byte, 8)
	for i := 0; i < 8; i++ {
		ret[i] = caseByBool(e.Word[i], (b&maskByI(uint(i))) != 0)
	}
	return ret
}

func (e *Enc) DecodeChar(s []byte) byte {
	if len(s) != 8 {
		panic("invalid length")
	}
	b := byte(0)
	for i, r := range s {
		if r < 0x5B {
			b ^= maskByI(uint(i))
		}
	}
	return b
}

func (e *Enc) EncodeString(s []byte) []byte {
	ret := make([]byte, len(s)*8)
	for i, b := range s {
		copy(ret[i*8:], e.EncodeChar(b))
	}
	return ret
}

func (e *Enc) DecodeString(s []byte) []byte {
	if len(s)%8 != 0 {
		panic("invalid length")
	}
	l := len(s) / 8
	ret := make([]byte, l)
	for i := 0; i < l; i++ {
		ret[i] = e.DecodeChar(s[i*8 : (i+1)*8])
	}
	return ret
}
